var colors = require('colors'),
    spawn = require('child_process').spawn,
    events = require('events'),
    util = require('util');

var encoder = exports;

var SAMPLE_SIZE = 16,    // 16-bit samples, Little-Endian, Signed
    CHANNELS = 2,        // 2 channels (left and right)
    SAMPLE_RATE = 44100, // 44,100 Hz sample rate.
    BLOCK_ALIGN = SAMPLE_SIZE / 8 * CHANNELS, // Number of 'Bytes per Sample'
    BYTES_PER_SECOND = SAMPLE_RATE * BLOCK_ALIGN;

encoder.bytesPerSecond = BYTES_PER_SECOND;

encoder.Encoder = function(req, res) {
    events.EventEmitter.call(this);

    console.log('New stream'.grey);
    var self = this;

    this.dataCallback = function(chunk) {
        if (self.encoder && self.encoder.stdin.writable) {
            self.encoder.stdin.write(chunk);
        }
    };

    var contentType = 'audio/mpeg',
        spawnName = 'lame',
        spawnOpts = [
            "-S", // Silent
            "-r", // Input is raw PCM
            "-s", SAMPLE_RATE / 1000, // Input sampling rate: 44,100
            "-", // Input from stdin
            "-" // Output to stdout
        ];

    var headers = {
        "Content-Type": contentType,
        "Connection": "close",
        "Transfer-Encoding": "identity"
    };
    res.writeHead(200, headers);

    this.encoder = spawn(spawnName, spawnOpts);
    this.encoder.stdout.on("data", function(chunk) {
        res.write(chunk);
    });

    console.log((spawnName + " Client Connected: " + req.connection.remoteAddress).green.bold);

    req.connection.on("close", function() {
        self.emit('finished');
        console.log((spawnName + " Client Disconnected: " + req.connection.remoteAddress).red.bold);
    });
};

util.inherits(encoder.Encoder, events.EventEmitter);

encoder.Encoder.prototype.connectStream = function(stdout) {
    if (stdout) {
        console.log('Connecting stream'.green);
        stdout.on('data', this.dataCallback);
    }
};

encoder.Encoder.prototype.disconnectStream = function(stdout) {
    if (stdout) {
        console.log('Disconnecting stream'.red);
        stdout.removeListener('data', this.dataCallback);
    }
};
