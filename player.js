var spawn = require('child_process').spawn,
    throttle = require('throttle'),
    encoder = require('./encoder'),
    events = require('events'),
    util = require('util');

var player = exports;

player.Player = function() {
    events.EventEmitter.call(this);

    this.playing = false;
    this.clients = [];
    this.songs = [];

    this.nextSong();
};

util.inherits(player.Player, events.EventEmitter);

player.Player.prototype.addEncoder = function(encoder) {
    var thisPlayer = this;

    if (this.currentSong) {
        encoder.connectStream(this.currentSong.stdout);
    }
    this.clients.push(encoder);

    encoder.on('finished', function() {
        if (thisPlayer.currentSong) {
            this.disconnectStream(thisPlayer.currentSong.stdout);
        }
        thisPlayer.clients.splice(thisPlayer.clients.indexOf(this), 1); // TODO: Find a nicer line for this
    });
};

player.Player.prototype.addSong = function(song) {
    this.songs.push(song);

    if (!this.playing) {
        this.nextSong();
    }
};

player.Player.prototype.nextSong = function() {
    var thisPlayer = this;

    if (this.songs.length !== 0) {
        this.playing = true;

        if (!this.playing && this.currentSong) {
            console.log('Killing silence');
            this.currentSong.kill();
        }

        console.log('Starting new song');
        
        var song = this.songs[0];
        var songPath = __dirname + '/music/' + song.id + '.mp3';
        var otherparams = ['-i', songPath, '-f', 's16le', '-acodec', 'pcm_s16le', '-ac', '2', '-ar', '44100', '-'];
        this.currentSong = spawn('ffmpeg', otherparams);
        this.currentSong.on('exit', function (code) {
            console.log('Song finished');
            
            if (thisPlayer.clients.length !== 0) {
                for (var i = 0; i < thisPlayer.clients.length; i++) {
                    thisPlayer.clients[i].disconnectStream(thisPlayer.currentSong.stdout);
                }
            }

            var finishedSong = thisPlayer.songs.shift();
            thisPlayer.emit('newSong', finishedSong);

            thisPlayer.nextSong();
        });
        throttle(this.currentSong.stdout, encoder.bytesPerSecond);

        if (this.clients.length !== 0) {
            for (var i = 0; i < this.clients.length; i++) {
                this.clients[i].connectStream(this.currentSong.stdout);
            }
        }
    }
    else {
        console.log('Nothing left to play'.bold);
        this.playing = false;

        var params = ['-i', __dirname + '/silence.mp3', '-f', 's16le', '-acodec', 'pcm_s16le', '-ac', '2', '-ar', '44100', '-'];
        this.currentSong = spawn('ffmpeg', params);
        this.currentSong.on('exit', function (code) {
            console.log('Silence finished');
        });
        throttle(this.currentSong.stdout, encoder.bytesPerSecond);

        if (this.clients.length !== 0) {
            for (var j = 0; j < this.clients.length; j++) {
                this.clients[j].connectStream(this.currentSong.stdout);
            }
        }
    }
};