var fs = require('fs'),
    request = require('request'),
    events = require('events'),
    util = require('util');

var downloader = exports;

downloader.Downloader = function(httpPath, downloadPath) {
    events.EventEmitter.call(this);
    
    this.httpPath = httpPath;
    this.downloadPath = downloadPath;
};

util.inherits(downloader.Downloader, events.EventEmitter);

downloader.Downloader.prototype.begin = function() {
    var thisDownloader = this;

    var file = fs.createWriteStream(this.downloadPath, {
        flags: 'w'
    });

    var req = request(this.httpPath);
    req.on('data', function(chunk) {
        file.write(chunk);
    });
    req.on('end', function() {
        file.end();
        thisDownloader.emit('done');
    });
};
