RITUNE = {};

RITUNE.appController = (function() {
	var

	init = function() {
		_.extend(this, Backbone.Events);

		// Set up our stores
		RITUNE.songStore = new RITUNE.collections.SongStore();

		// Initialise our controllers
		RITUNE.ajaxController.init();
		RITUNE.viewController.init();
		RITUNE.dataController.init();

		// Create our views
		RITUNE.homeView = new RITUNE.views.HomeView({
			el: '#homeView',
			model: RITUNE.models.Song,
			collection: RITUNE.songStore
		});

		// Create the router
		RITUNE.router = new RITUNE.routers.Router();

		RITUNE.songStore.fetch();

		// And start Backbone history
		Backbone.history.start();
	};

	return {
		init : init
	};
}());
