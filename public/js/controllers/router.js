RITUNE.routers = {};

RITUNE.routers.Router = Backbone.Router.extend({
    routes: {
        '': 'showHome',
        '/home': 'showHome'
    },
    
    initialize: function (options) {
        _.bindAll(this, 'showHome');
    },
    
    // Home
    showHome: function() {
        
    }
});