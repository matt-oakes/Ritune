RITUNE.dataController = (function() {
    var

    init = function() {
        _.extend(this, Backbone.Events);

        // Makes the client refresh its data as soon as the server says it's playing a new song
        var socket = io.connect('http://localhost:5555');
        socket.on('newSong', function() {
            RITUNE.songStore.remove(RITUNE.songStore.at(0));
        });
    };

    return {
        init: init
    };
    
}());