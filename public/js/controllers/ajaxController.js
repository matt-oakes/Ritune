RITUNE.ajaxController = (function() {
    var

    init = function() {
        _.extend(this, Backbone.Events);
    },

    getSongs = function() {
        var self = this;

        //Get restaurants
        $.get('/songs', function(data) {
            var collection = JSON.parse(data);
            self.trigger('AJAX_CONTROLLER_SONG_DATA_READY', collection);
        });
    },

    addSong = function(artist, title, url) {
        var source = _getSource(url);

        if (source === 'soundcloud') {
            var requestUrl = 'http://api.soundcloud.com/resolve.json?url=' + url + '&client_id=10f363c11d1493b4c345b12e982c7c54';
            $.ajax({
                url: requestUrl,
                dataType: 'jsonp',
                success: function(data) {
                    _newSong(artist, title, data.stream_url + '?client_id=10f363c11d1493b4c345b12e982c7c54');
                }
            });
        }
        else if (source === 'youtube') {
            var youtube = new RegExp('(http://www.youtube.com/watch\\?v\\=)', 'g');
            var youTubeId = url.replace(youtube, '').split('&')[0];
            console.log('YouTube: ' + youTubeId);

            $.ajax({
                url: 'youtube/' + youTubeId,
                success: function(data) {
                    // Regexs from https://github.com/Cosmologist/Java-Youtube-Download-Links/blob/master/src/com/appfactory/ydl/YDL.java
                    var regex1 = new RegExp('stream_map=(.[^&]*?)&');
                    var regex2 = new RegExp('^(.*?)\\\\u0026');
                    var regex3 = new RegExp('url=(.*?)&.*?itag=([0-9]+)');

                    var urlStuff = regex1.exec(data);
                    var justTheStreams = decodeURIComponent(regex2.exec(urlStuff[1])[1].replace(/\+/g,  " "));
                    var streams = justTheStreams.split(',');
                    for (var i = 0; i < streams.length; i++) {
                        var streamUrl = streams[i].match(regex3);

                        // TODO: Pick the best quality possible but don't require a certain one
                        if (streamUrl[2] === "18") {
                            _newSong(artist, title, decodeURIComponent(streamUrl[1]));
                            return;
                        }
                    }
                }
            });
        }
        else {
            alert('Unknown URL');
        }
    },

    _newSong = function(artist, title, url) {
        var model = new RITUNE.models.Song({
            artist: artist,
            title: title,
            url: url
        });
        RITUNE.songStore.add(model);
        model.save(['artist', 'title', 'url']);
    },

    _getSource = function(url) {
        var soundcloud = new RegExp("^(http://soundcloud.com/)[a-zA-Z0-9-_]+\/[a-zA-Z0-9-_]+$");
        var youtube = new RegExp("^(http://www.youtube.com/watch\\?v\\=).+$");

        if (soundcloud.test(url)) {
            return "soundcloud";
        }
        else if (youtube.test(url)) {
            return "youtube";
        }
        else {
            return null;
        }
    };

    return {
        init: init,
        getSongs: getSongs,
        addSong: addSong
    };

}());
