RITUNE.collections = {};

RITUNE.collections.SongStore = Backbone.Collection.extend({
    url: '/songs',
    model: RITUNE.models.Song,

    initialize: function() {
    }
});
