RITUNE.views = RITUNE.views || {};

RITUNE.views.HomeView = Backbone.View.extend({
    _currentNewIndex: 0,
    _currentViewIndex: 0,

    events: {
        'click button#add': 'handleAdd'
    },

    template: _.template($('#homeViewTemplate').html()),
    songTemplate: _.template($('#songTemplate').html()),
    
    initialize: function() {
        _.bindAll(this, 'render', 'addOne', 'advanceOne', 'handleAdd');
        this.collection.bind('reset', this.render);
        this.collection.bind('add', this.addOne);
        this.collection.bind('remove', this.advanceOne);
    },

    render: function() {
        this.$el.html(this.template());

        var currentOpacity = 0.5;
        this.collection.forEach(function(song, i) {
            var element = $('<div></div>');
            var view = new RITUNE.views.SongView({
                el: element,
                model: song
            });
            this.$el.find('#content').append(element);

            view.changePosition(this._currentNewIndex);
            this._currentNewIndex -= 1000;

            if (i > 0) {
                view.changeOpacity(currentOpacity);
                if (currentOpacity !== 0) {
                    currentOpacity = Math.round((currentOpacity - 0.1) * 10) / 10;
                }
            }
            else {
                view.changeOpacity(1);
            }
        }, this);

        return this;
    },

    addOne: function() {
        var song = this.collection.at(this.collection.length - 1);

        var element = $('<div></div>');
        var view = new RITUNE.views.SongView({
            el: element,
            model: song
        });
        this.$el.find('#content').append(element);

        if (this.collection.length > 1) {
            var currentOpacity = 0.5;
            this.collection.forEach(function(song, i) {
                if (currentOpacity !== 0) {
                    currentOpacity = Math.round((currentOpacity - 0.1) * 10) / 10;
                }
            }, this);

            view.changeOpacity(currentOpacity);
        }
        else {
            view.changeOpacity(1);
        }

        view.changePosition(this._currentNewIndex);
        this._currentNewIndex -= 1000;
    },

    advanceOne: function() {
        this._currentViewIndex += 1000;
        $(this.el).find('#content').css({
            '-webkit-transform': 'translateZ(' + this._currentViewIndex + 'px)'
        });

        var items = $(this.el).find('div.item');
        var oldElement = items.first().parent();

        var currentOpacity = 0.5;
        items.each(function(i, item) {
            if (i > 1) {
                $(item).css({'opacity': currentOpacity});

                if (currentOpacity !== 0) {
                    currentOpacity = Math.round((currentOpacity - 0.1) * 10) / 10;
                }
            }
            else if (i === 1) {
                $(item).css({'opacity': 1});
            }
        });

        setTimeout(function() {
            oldElement.remove();
        }, 1100);
    },

    handleAdd: function(evt) {
        var thisView = this;
        
        var artist = $('input#artist').val();
        var title = $('input#title').val();
        var url = $('input#url').val();
        RITUNE.ajaxController.addSong(artist, title, url);
    }

});
