RITUNE.views = RITUNE.views || {};

RITUNE.views.SongView = Backbone.View.extend({
    template: _.template($('#songTemplate').html()),
    className: 'item',
    tagName: 'div',
    
    initialize: function() {
        _.bindAll(this, 'render', 'change', 'changePosition', 'changeOpacity');
        this.model.bind('change', this.change);

        this.render();
    },

    render: function() {
        this.$el.html(this.template({song: this.model.toJSON()}));
    },

    change: function() {
        this.$el.find('p.artist').html(this.model.get('artist'));
        this.$el.find('p.title').html(this.model.get('title'));
    },

    changePosition: function(newZ) {
        this.$el.find('div.item').css({'-webkit-transform': 'translateZ(' + newZ + 'px)'});
    },

    changeOpacity: function(newOpacity) {
        this.$el.find('div.item').css({'opacity': newOpacity});
    }

});
