var playerModule = require('./player'),
    encoder = require('./encoder'),
    express = require('express'),
    socketio = require('socket.io'),
    downloader = require('./downloader'),
    request = require('request'),
    fs = require('fs');

var server = express.createServer(),
    io = socketio.listen(server),
    player = new playerModule.Player();

server.use(express.bodyParser());

server.get('/stream.mp3', function(req, res) {
    player.addEncoder(new encoder.Encoder(req, res));
});

// Get all songs
server.get('/songs', function(req, res) {
    res.send(player.songs);
});

// Post a new song
server.post('/songs', function(req, res) {
    var song = req.body;

    var id = Math.floor(Math.random() * 100000001);
    song.id = id;

    var down = new downloader.Downloader(song.url, __dirname + '/music/' + id + '.mp3');
    down.on('done', function() {
        player.addSong(song);
    });
    down.begin();

    res.send({id: id});
});

// Takes in a youtube id and returns a list of stream urls
server.get('/youtube/:id', function(req, res) {
    var youtubeId = req.params.id;

    request('http://youtube.com/watch?v=' + youtubeId, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            res.send(body);
        }
    });
});

// Get the public html app
server.get('/', function(req, res) {
    res.sendfile('public/index.html');
});

// Get a specific html file
// TODO: What if filename is invalid
server.get('*', function(req, res) {
    res.sendfile('public/' + req.params.toString());
});

server.listen(5555);

io.sockets.on('connection', function (socket) {
    player.on('newSong', function (song) {
        fs.unlink(__dirname + '/music/' + song.id + '.mp3');
        socket.emit('newSong');
    });
});